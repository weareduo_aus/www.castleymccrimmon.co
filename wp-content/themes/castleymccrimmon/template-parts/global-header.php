<?php
$page_background_colour = get_field('page_background_colour');
$address = get_field('address', 'option');
$email_address = get_field('email_address', 'option');
$phone_number = get_field('phone_number', 'option');
$instagram_url = get_field('instagram_url', 'option');
?>

<header data-component="global-header" <?php if ($page_background_colour == '6F7862'): ?>class="light-nav"<?php endif; ?>>
  <div class="container">
    <a href="/" data-component="logo">
      <?php get_template_part( 'template-parts/svgs/_svg', 'logo' ); ?>
    </a>
    <button data-component="nav-toggle">
      <svg>
        <use xmlns:xlink="http://www.w3.org/1999/xlink"
          xlink:href="<?php echo get_template_directory_uri(); ?>/assets/src/images/icons.svg#nav-toggle-icon">
        </use>
      </svg>
    </button>
    <div data-component="main-nav">
      <div class="wrapper">
        <nav>
        <?php
          wp_nav_menu(array(
            'menu' => 'main_navigation',
            'theme_location' => 'main_navigation',
            'depth' => 1,
            'container' => '',
            'container_class' => '',
            'menu_class' => '',
            'menu_id' => '',
            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'walker' => new Child_Wrap()
          )
        );
        ?>
        </nav>
        <footer>
          <ul>
            <li class="address">
              <?php echo $address; ?>
            </li>
            <li class="contact-details">
              <ul>
                <li><p><a href="mailto:<?php echo $email_address; ?>"><?php echo $email_address; ?></a></p></li>
                <li><p><a href="tel:<?php echo $phone_number; ?>"><?php echo $phone_number; ?></a></p></li>
                <li><p><a href="<?php echo $instagram_url; ?>" target="_blank">@castley_mccrimmon</a></p></li>
              </ul>
            </li>
          </ul>
        </footer>
      </div>
    </div>
  </div>
</header>