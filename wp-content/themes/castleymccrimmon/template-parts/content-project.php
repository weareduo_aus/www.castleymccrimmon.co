<?php
$project_year = get_field('project_year');
?>

<li class="project">
  <a href="<?php the_permalink(); ?>">
    <?php if( have_rows('project_our_work_images') ): ?>
      <ul class="project-images">
      <?php while( have_rows('project_our_work_images') ): the_row();
        $our_work_image = get_sub_field('our_work_image');
        ?>
        <li>
          <img src="<?php echo $our_work_image['url']; ?>" alt="<?php the_title(); ?>">
        </li>
      <?php endwhile; ?>
      </ul>
    <?php endif; ?>
    <ul class="project-details">
      <li class="project-title">
        <header>
          <h4><?php the_title(); ?></h4>
        </header>
      </li>
      <li class="project-date">
        <p><?php echo $project_year; ?></p>
      </li>
      <li class="view-project">
        <p>
          <span>View Project</span>
          <svg>
            <use xmlns:xlink="http://www.w3.org/1999/xlink"
              xlink:href="<?php echo get_template_directory_uri(); ?>/assets/src/images/icons.svg#arrow-icon">
            </use>
          </svg>
        </p>
      </li>
    </ul>
  </a>
</li>