<div data-component="project-gallery-overlay" id="sketches-gallery">
  <button data-component="close-sketches-gallery-overlay">
    <svg>
      <use xmlns:xlink="http://www.w3.org/1999/xlink"
        xlink:href="<?php echo get_template_directory_uri(); ?>/assets/src/images/icons.svg#nav-toggle-icon">
      </use>
    </svg>
  </button>
  <div class="wrapper">
    <?php if( have_rows('project_sketches_gallery_new') ): ?>
    <div data-component="gallery-overlay-slider">
      <div class="swiper sketches-gallery-overlay-slider">
        <div class="swiper-wrapper">
          <?php $total_images = count( get_field( 'project_sketches_gallery_new' ) ); ?>
          <?php while( have_rows('project_sketches_gallery_new') ): the_row();
              $project_sketches_gallery_image = get_sub_field('project_sketches_gallery_image');
              $project_sketches_gallery_image_title = get_sub_field('project_sketches_gallery_image_title');
              $project_sketches_gallery_image_orientation = get_sub_field('project_sketches_gallery_image_orientation');
              ?>

          <div class="swiper-slide <?php echo $project_sketches_gallery_image_orientation; ?>">
            <div class="image-wrapper">
              <img src="<?php echo $project_sketches_gallery_image['url']; ?>"
                alt="<?php echo $project_sketches_gallery_image_title; ?>">
              <div class="project-gallery-image-title">
                <p><?php echo get_row_index(); ?> / <?php echo $total_images; ?></p>
              </div>
            </div>
          </div>
          <?php endwhile; ?>
        </div>
        <button data-component="swiper-button-prev" class="slider-navigation-button"></button>
        <button data-component="swiper-button-next" class="slider-navigation-button"></button>
      </div>
    </div>
    <?php endif; ?>
  </div>
</div>