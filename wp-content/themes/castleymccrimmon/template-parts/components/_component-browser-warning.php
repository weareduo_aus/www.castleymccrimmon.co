<?php if (preg_match('~MSIE|Internet Explorer~i', $_SERVER['HTTP_USER_AGENT']) || (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false)): ?>
<div class="browser-warning">
  <div class="copy">
    <div class="site-logo">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/src/images/legacy-logo.png"
        alt="<?php bloginfo('name'); ?>">
    </div>
    <p>You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/?locale=en">upgrade
        your browser</a> to view our website properly.</p>
  </div>
</div>
<?php endif; ?>