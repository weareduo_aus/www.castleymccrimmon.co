<?php
$address = get_field('address', 'option');
$email_address = get_field('email_address', 'option');
$phone_number = get_field('phone_number', 'option');
$instagram_url = get_field('instagram_url', 'option');
?>

<footer data-component="global-footer">
  <div class="container">
    <div class="column logo">
      <button data-component="footer-scroll-btn">
        <?php get_template_part( 'template-parts/svgs/_svg', 'footer-logo' ); ?>
      </button>
    </div>
    <div class="column double">
      <div class="column address">
        <?php echo $address; ?>
      </div>
      <div class="column contact-links">
        <ul class="contact-details">
          <li><p><a href="mailto:<?php echo $email_address; ?>"><?php echo $email_address; ?></a></p></li>
          <li><p><a href="tel:<?php echo $phone_number; ?>"><?php echo $phone_number; ?></a></p></li>
          <li><p><a href="<?php echo $instagram_url; ?>" target="_blank">@castley_mccrimmon</a></p></li>
        </ul>
        <div class="links-credit">
          <div class="column links">
          <?php
            wp_nav_menu(array(
              'menu' => 'footer_navigation',
              'theme_location' => 'footer_navigation',
              'depth' => 1,
              'container' => '',
              'container_class' => '',
              'menu_class' => '',
              'menu_id' => '',
              'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
              'walker' => new Child_Wrap()
            )
          );
          ?>
          </div>
          <div class="column credits">
            <ul>
              <li><p>Copyright <?php echo date('Y'); ?></p></li>
              <li><p>Built By <a href="https://alter.com.au/" target="_blank">Alter</a></p></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>