<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="128px"
	 height="128px" viewBox="0 0 128 128" style="enable-background:new 0 0 128 128;" xml:space="preserve">
  <g id="circle">
	  <path d="M64,0c35.3,0,64,28.7,64,64s-28.7,64-64,64S0,99.3,0,64S28.7,0,64,0z"/>
  </g>
  <g id="arrow">
	  <path d="M92.7,61.4c2,1.2,2,4.1,0,5.2L48.3,92.3c-2,1.2-4.5-0.3-4.5-2.6V38.4c0-2.3,2.5-3.8,4.5-2.6L92.7,61.4z"/>
  </g>
</svg>