<section data-component="heading-image-module" class="default-module <?php if ($heading_image_module_has_border): ?>has-border<?php endif; ?> <?php if ($heading_image_module_smaller_top_padding): ?>smaller-padding-top<?php endif; ?> <?php if ($heading_image_module_smaller_bottom_padding): ?>smaller-padding-bottom<?php endif; ?>">
  <div class="container">
    <div class="column text">
      <div class="inner">
        <header>
          <h2 class="module-heading"><?php echo $heading_image_module_heading; ?></h2>
        </header>
        <?php if ($heading_image_module_sub_heading): ?>
          <div class="module-introduction">
            <?php echo $heading_image_module_sub_heading; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
    <div class="column image">
      <img src="<?php echo $heading_image_module_image['url']; ?>" width="<?php echo $heading_image_module_image['width']; ?>" height="<?php echo $heading_image_module_image['height']; ?>" alt="<?php echo $heading_image_module_image['alt']; ?>">
    </div>
  </div>
</section>