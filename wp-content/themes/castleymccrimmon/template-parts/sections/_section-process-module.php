<section data-component="process-module" class="default-module">
  <div class="container">
    <div class="column text">
      <div class="inner">
        <header>
          <h2 class="module-heading">Process</h2>
        </header>
      </div>
    </div>
    <div class="column processes">
      <?php if( have_rows('processes') ): ?>
      <div data-component="accordion">
        <?php while( have_rows('processes') ): the_row();
          $process_title = get_sub_field('process_title');
          $process_text = get_sub_field('process_text');
          ?>

        <div class="accordion-wrapper">
          <div class="accordion">
            <h4><?php echo $process_title; ?></h4>
            <span class="close" title="Close overlay">
              <svg>
                <use xmlns:xlink="http://www.w3.org/1999/xlink"
                  xlink:href="<?php echo get_template_directory_uri(); ?>/assets/src/images/icons.svg#nav-toggle-icon">
                </use>
              </svg>
            </span>
          </div>
          <div class="panel">
            <div class="content-inner">
              <?php echo $process_text; ?>
            </div>
          </div>
        </div>
        <?php endwhile; ?>
      </div>
      <?php endif; ?>
    </div>
  </div>
</section>