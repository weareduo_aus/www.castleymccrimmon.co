<?php
$projects_hero_image = get_field('projects_hero_image', 'option');
$projects_hero_image_title = get_field('projects_hero_image_title', 'option');
$project_hero_text_colour = get_field('project_hero_text_colour');
$projects_hero_text_colour = get_field('projects_hero_text_colour', 'option');
?>

<section data-component="full-screen-page-hero">
  <?php if (is_singular('projects')): ?>
    <?php the_post_thumbnail(); ?>
  <?php else : ?>
    <img src="<?php echo $projects_hero_image['url']; ?>" alt="<?php echo $projects_hero_image_title; ?>">
  <?php endif; ?>
  <?php if (is_singular('projects')): ?>
    <div class="full-screen-page-hero-title" <?php if ($project_hero_text_colour): ?>style="color: <?php echo $project_hero_text_colour; ?>"<?php endif; ?>>
      <p><?php the_title(); ?></p>
    </div>
  <?php else : ?>
    <?php if ($projects_hero_image_title): ?>
      <div class="full-screen-page-hero-title" <?php if ($projects_hero_text_colour): ?>style="color: <?php echo $projects_hero_text_colour; ?>"<?php endif; ?>>
        <p><?php echo $projects_hero_image_title; ?></p>
      </div>
    <?php endif; ?>
  <?php endif; ?>
  <?php get_template_part( 'template-parts/components/_component', 'scroll-btn' ); ?>
</section>