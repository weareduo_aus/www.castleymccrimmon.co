<?php
$default_page_hero_heading = get_field('default_page_hero_heading');
$default_page_hero_text = get_field('default_page_hero_text');
$default_page_hero_image = get_field('default_page_hero_image');
?>

<section data-component="page-hero">
  <div class="container">
    <div class="column text">
      <div class="inner">
        <header class="page-title">
          <h1><?php echo $default_page_hero_heading; ?></h1>
        </header>
        <div class="page-introduction">
          <?php echo $default_page_hero_text; ?>
        </div>
      </div>
    </div>
    <div class="column image">
      <img src="<?php echo $default_page_hero_image['url']; ?>" width="<?php echo $default_page_hero_image['width']; ?>" height="<?php echo $default_page_hero_image['height']; ?>" alt="<?php echo $default_page_hero_heading; ?>">
    </div>
  </div>
</section>