<section data-component="testimonials-module" class="default-module">
  <div class="container">
    <div class="column text">
      <div class="inner">
        <header>
          <h2 class="module-heading">Testimonials</h2>
        </header>
        <div data-component="slider-navigation">
          <button data-component="swiper-button-prev" class="slider-navigation-button">
            <svg>
              <use xmlns:xlink="http://www.w3.org/1999/xlink"
                xlink:href="<?php echo get_template_directory_uri(); ?>/assets/src/images/icons.svg#arrow-icon">
              </use>
            </svg>
          </button>
          <button data-component="swiper-button-next" class="slider-navigation-button">
            <svg>
              <use xmlns:xlink="http://www.w3.org/1999/xlink"
                xlink:href="<?php echo get_template_directory_uri(); ?>/assets/src/images/icons.svg#arrow-icon">
              </use>
            </svg>
          </button>
        </div>
      </div>
    </div>
    <div class="column testimonials">
      <?php if( have_rows('testimonials') ): ?>
      <div data-component="testimonials-slider">
        <div class="swiper">
          <div class="swiper-wrapper">
            <?php while( have_rows('testimonials') ): the_row();
              $testimonial_testimonial = get_sub_field('testimonial_testimonial');
              $testimonial_name = get_sub_field('testimonial_name');
              $testimonial_business = get_sub_field('testimonial_business');
            ?>

            <div class="swiper-slide">
              <div class="testimonial">
                <?php echo $testimonial_testimonial; ?>
              </div>
              <ul>
                <li><p><?php echo $testimonial_name; ?></p></li>
                <li><p><?php echo $testimonial_business; ?></p></li>
              </ul>
            </div>

            <?php endwhile; ?>
          </div>
        </div>
      </div>
      <?php endif; ?>
    </div>
  </div>
</section>