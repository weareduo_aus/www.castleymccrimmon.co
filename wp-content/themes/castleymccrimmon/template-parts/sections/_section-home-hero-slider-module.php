<section data-component="home-hero-slider-module">
  <?php if( have_rows('homepage_hero_slides') ): ?>
  <div data-component="slider">
    <div class="swiper">
      <div class="swiper-wrapper">
        <?php while( have_rows('homepage_hero_slides') ): the_row();
        $homepage_hero_slide = get_sub_field('homepage_hero_slide');
        $project_hero_text_colour = get_field('project_hero_text_colour', $homepage_hero_slide->ID);
        $project_hero_slider_image = get_field('project_hero_slider_image', $homepage_hero_slide->ID);
        ?>

        <div class="swiper-slide">
          <img src="<?php echo $project_hero_slider_image['url']; ?>" width="<?php echo $project_hero_slider_image['width']; ?>" height="<?php echo $project_hero_slider_image['height']; ?>" alt="<?php echo $homepage_hero_slide->post_title; ?>">
          <div class="full-screen-page-hero-title" style="color: <?php echo $project_hero_text_colour; ?>">
            <p><?php echo $homepage_hero_slide->post_title; ?></p>
          </div>
        </div>

        <?php endwhile; ?>
      </div>
      <?php get_template_part( 'template-parts/components/_component', 'scroll-btn' ); ?>
      <div data-component="slider-navigation">
        <button data-component="swiper-button-prev" class="slider-navigation-button">
          <svg>
            <use xmlns:xlink="http://www.w3.org/1999/xlink"
              xlink:href="<?php echo get_template_directory_uri(); ?>/assets/src/images/icons.svg#arrow-icon">
            </use>
          </svg>
        </button>
        <button data-component="swiper-button-next" class="slider-navigation-button">
          <svg>
            <use xmlns:xlink="http://www.w3.org/1999/xlink"
              xlink:href="<?php echo get_template_directory_uri(); ?>/assets/src/images/icons.svg#arrow-icon">
            </use>
          </svg>
        </button>
      </div>
    </div>
  </div>
  <?php endif; ?>
</section>