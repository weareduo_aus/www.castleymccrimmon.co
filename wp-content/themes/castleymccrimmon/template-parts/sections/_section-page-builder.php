<?php if( have_rows('modules') ): ?>
  <?php while( have_rows('modules') ): the_row(); ?>
    <?php if( get_row_layout() == 'heading_text_module' ):

      $heading_text_module_heading = get_sub_field('heading_text_module_heading');
      $heading_text_module_sub_heading = get_sub_field('heading_text_module_sub_heading');
      $heading_text_module_text = get_sub_field('heading_text_module_text');
      $heading_text_module_has_border = get_sub_field('heading_text_module_has_border');
      $heading_text_module_smaller_top_padding = get_sub_field('heading_text_module_smaller_top_padding');
      $heading_text_module_smaller_bottom_padding = get_sub_field('heading_text_module_smaller_bottom_padding');

      set_query_var( 'heading_text_module_heading', $heading_text_module_heading );
      set_query_var( 'heading_text_module_sub_heading', $heading_text_module_sub_heading );
      set_query_var( 'heading_text_module_text', $heading_text_module_text );
      set_query_var( 'heading_text_module_has_border', $heading_text_module_has_border );
      set_query_var( 'heading_text_module_smaller_top_padding', $heading_text_module_smaller_top_padding );
      set_query_var( 'heading_text_module_smaller_bottom_padding', $heading_text_module_smaller_bottom_padding );


      ?>

      <?php get_template_part( 'template-parts/sections/_section', 'heading-text-module' ); ?>

      <?php elseif( get_row_layout() == 'heading_image_module' ):

      $heading_image_module_heading = get_sub_field('heading_image_module_heading');
      $heading_image_module_sub_heading = get_sub_field('heading_image_module_sub_heading');
      $heading_image_module_image = get_sub_field('heading_image_module_image');
      $heading_image_module_has_border = get_sub_field('heading_image_module_has_border');
      $heading_image_module_smaller_top_padding = get_sub_field('heading_image_module_smaller_top_padding');
      $heading_image_module_smaller_bottom_padding = get_sub_field('heading_image_module_smaller_bottom_padding');

      set_query_var( 'heading_image_module_heading', $heading_image_module_heading );
      set_query_var( 'heading_image_module_sub_heading', $heading_image_module_sub_heading );
      set_query_var( 'heading_image_module_image', $heading_image_module_image );
      set_query_var( 'heading_image_module_has_border', $heading_image_module_has_border );
      set_query_var( 'heading_image_module_smaller_top_padding', $heading_image_module_smaller_top_padding );
      set_query_var( 'heading_image_module_smaller_bottom_padding', $heading_image_module_smaller_bottom_padding );

      ?>

      <?php get_template_part( 'template-parts/sections/_section', 'heading-image-module' ); ?>

    <?php elseif( get_row_layout() == 'team_module' ): ?>

      <?php get_template_part( 'template-parts/sections/_section', 'team-module' ); ?>

    <?php elseif( get_row_layout() == 'process_module' ): ?>

      <?php get_template_part( 'template-parts/sections/_section', 'process-module' ); ?>

    <?php elseif( get_row_layout() == 'testimonials_module' ): ?>

      <?php get_template_part( 'template-parts/sections/_section', 'testimonials-module' ); ?>

    <?php elseif( get_row_layout() == 'our_work_module' ): ?>

      <?php get_template_part( 'template-parts/sections/_section', 'projects-module' ); ?>

    <?php endif; ?>
  <?php endwhile; ?>
<?php endif; ?>
