<section data-component="heading-text-module" class="default-module <?php if ($heading_text_module_has_border): ?>has-border<?php endif; ?> <?php if ($heading_text_module_smaller_top_padding): ?>smaller-padding-top<?php endif; ?> <?php if ($heading_text_module_smaller_bottom_padding): ?>smaller-padding-bottom<?php endif; ?>">
  <div class="container">
    <div class="column text">
      <div class="inner">
        <header>
          <h2 class="module-heading"><?php echo $heading_text_module_heading; ?></h2>
        </header>
        <?php if ($heading_text_module_sub_heading): ?>
          <div class="module-introduction">
            <?php echo $heading_text_module_sub_heading; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
    <div class="column large-text">
      <?php echo $heading_text_module_text; ?>
      <?php if( have_rows('heading_text_module_ctas') ): ?>
        <ul class="ctas">
        <?php while( have_rows('heading_text_module_ctas') ): the_row();
        $cta = get_sub_field('cta');
        $cta_url = $cta['url'];
        $cta_title = $cta['title'];
        $cta_target = $cta['target'] ? $cta['target'] : '_self';
        ?>

        <li>
          <a href="<?php echo esc_url( $cta_url ); ?>" target="<?php echo esc_attr( $cta_target ); ?>"><?php echo esc_html( $cta_title ); ?></a>
        </li>

        <?php endwhile; ?>
        </ul>
      <?php endif; ?>
    </div>
  </div>
</section>