<section data-component="projects-module" class="default-module">
  <div class="container">
    <div class="column text">
      <div class="inner">
        <header>
          <h2 class="module-heading"><a href="/projects/">Our Work</a></h2>
        </header>
      </div>
    </div>
    <div class="column projects">
      <header>
        <h3 class="module-heading">Featured Projects</h3>
      </header>
      <ul class="projects">
      <?php while( have_rows('homepage_projects') ): the_row();
        $homepage_project = get_sub_field('homepage_project');
        $project_year = get_field('project_year', $homepage_project->ID);
        ?>

        <li class="project">
          <a href="<?php the_permalink($homepage_project->ID); ?>">
            <div class="project-image">
              <?php echo get_the_post_thumbnail( $homepage_project, 'full' ); ?>
            </div>
            <ul class="project-details">
              <li class="project-title">
                <header>
                  <h4><?php echo $homepage_project->post_title; ?></h4>
                </header>
              </li>
              <li class="project-date">
                <p><?php echo $project_year; ?></p>
              </li>
              <li class="view-project">
                <p>
                  <span>View Project</span>
                  <svg>
                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                      xlink:href="<?php echo get_template_directory_uri(); ?>/assets/src/images/icons.svg#arrow-icon">
                    </use>
                  </svg>
                </p>
              </li>
            </ul>
          </a>
        </li>
        <?php endwhile; ?>
      </ul>
    </div>
  </div>
</section>