<section data-component="team-module" class="default-module">
  <div class="container">
    <div class="column text">
      <div class="inner">
        <header>
          <h2 class="module-heading">Who are Castley McCrimmon?</h2>
        </header>
      </div>
    </div>
    <div class="column team-members">
      <?php if( have_rows('team_module_team_members') ): ?>
      <ul class="team-members">
        <?php while( have_rows('team_module_team_members') ): the_row();
        $team_member_image = get_sub_field('team_member_image');
        $team_member_name = get_sub_field('team_member_name');
        $team_member_role = get_sub_field('team_member_role');
        $team_member_bio = get_sub_field('team_member_bio');
        ?>
        <li>
          <div class="team-member-image">
            <img src="<?php echo $team_member_image['url']; ?>" width="<?php echo $team_member_image['width']; ?>"
              height="<?php echo $team_member_image['height']; ?>" alt="<?php echo $team_member_name; ?>">
          </div>
          <ul>
            <li>
              <header><h3><?php echo $team_member_name; ?></h3></header>
            </li>
            <li>
              <header><h4><?php echo $team_member_role; ?></h4></header>
            </li>
          </ul>
          <div class="team-member-bio">
            <?php echo $team_member_bio; ?>
          </div>
        </li>
        <?php endwhile; ?>
      </ul>
      <?php endif; ?>
    </div>
  </div>
</section>