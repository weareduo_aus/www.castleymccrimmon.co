<?php
/**
* Template Name: Contact
*/

$address = get_field('address', 'option');
$email_address = get_field('email_address', 'option');
$phone_number = get_field('phone_number', 'option');
$instagram_url = get_field('instagram_url', 'option');

get_header(); ?>

<section data-component="contact-module">
  <div class="container">
    <div class="inner">
      <?php echo $address; ?>
      <p>P: <a href="tel:<?php echo $phone_number; ?>"><?php echo $phone_number; ?></a></p>
      <p>E: <a href="mailto:<?php echo $email_address; ?>"><?php echo $email_address; ?></a></p>
      <p><a href="<?php echo $instagram_url; ?>" target="_blank">Instagram</a></p>
    </div>
  </div>
</section>

<?php get_footer(); ?>
