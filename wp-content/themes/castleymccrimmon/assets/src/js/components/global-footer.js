import { gsap } from 'gsap'
import { ScrollToPlugin } from 'gsap/dist/ScrollToPlugin.js'
gsap.registerPlugin(ScrollToPlugin)

/**
 * GlobalFooter functionality.
 * @author Dayne Coulson-Hoffacker
 */

export default class GlobalFooter {
  /**
     * Constructs a new GlobalFooter instance.
     * @param {Object} options - Options.
     * @param {HTMLElement} options.element - Container.
     */

  constructor(options) {
    this.element = options.element
    this.footerScrollBtn = this.element.querySelector('[data-component="footer-scroll-btn"]')

    this.footerScrollBtn.addEventListener('click', () => {
      gsap.to(window, {duration: 1, ease: 'power2', scrollTo: {y: 0 }})
    })

  }
}
