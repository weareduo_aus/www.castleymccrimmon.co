import { Swiper, Navigation } from 'swiper'

/**
 * TestimonialsSlider functionality.
 * @author Dayne Coulson-Hoffacker
 */

export default class TestimonialsSlider {
  /**
     * Constructs a new TestimonialsSlider instance.
     * @param {Object} options - Options.
     * @param {HTMLElement} options.element - Container.
     */

  constructor(options) {
    this.element = options.element
    this.swiper = this.element.querySelector('.swiper')
    this.swiperButtonPrev = document.querySelector('[data-component="testimonials-module"] [data-component="swiper-button-prev"]')
    this.swiperButtonNext = document.querySelector('[data-component="testimonials-module"] [data-component="swiper-button-next"]')

    var slider = new Swiper(this.swiper, {
      loop: true,
      slidesPerView: 1,
      breakpoints: {
        667: {
          slidesPerView: 2,
          spaceBetween: 48,
        },
        1024: {
          slidesPerView: 1,
          spaceBetween: 48,
        },
        1180: {
          slidesPerView: 2,
          spaceBetween: 48,
        }
      }
    })

    if (this.swiperButtonPrev) {
      this.swiperButtonPrev.addEventListener('click', () => {
        slider.slidePrev()
      })
    }

    if (this.swiperButtonNext) {
      this.swiperButtonNext.addEventListener('click', () => {
        slider.slideNext()
      })
    }

  }
}
