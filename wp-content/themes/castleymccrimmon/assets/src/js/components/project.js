import Swiper from 'swiper/bundle'

/**
 * Project functionality.
 * @author Dayne Coulson-Hoffacker
 */

export default class Project {
  /**
     * Constructs a new Project instance.
     * @param {Object} options - Options.
     * @param {HTMLElement} options.element - Container.
     */

  constructor(options) {
    this.element = options.element

    // Variables
    this.projectSliderSlider = this.element.querySelector('.project-slider')
    this.projectSliderSlides = this.projectSliderSlider.querySelectorAll('.swiper-slide')
    this.projectSliderPrevBtn = this.projectSliderSlider.querySelector('[data-component="swiper-button-prev"]')
    this.projectSliderNextBtn = this.projectSliderSlider.querySelector('[data-component="swiper-button-next"]')
    this.projectOverlay = this.element.querySelector('[data-component="project-gallery-overlay"]#project-gallery')
    this.projectOverlaySlider = this.projectOverlay.querySelector('.gallery-overlay-slider')
    this.projectOverlaySliderPrevBtn = this.projectOverlay.querySelector('[data-component="swiper-button-prev"]')
    this.projectOverlaySliderNextBtn = this.projectOverlay.querySelector('[data-component="swiper-button-next"]')
    this.projectOverlayCloseBtns = this.projectOverlay.querySelectorAll('[data-component="close-project-gallery-overlay"]')
    this.SketchesSliderSwiper = this.element.querySelector('.project-sketches-slider')

    var projectSlider = new Swiper(this.projectSliderSlider, {
      loop: true,
      slidesPerView: "auto",
      spaceBetween: 16,
      breakpoints: {
        1024: {
          spaceBetween: 32
        },
        1440: {
          spaceBetween: 48
        }
      }
    })

    var projectOverlaySlider = new Swiper(this.projectOverlaySlider, {
      slidesPerView: "auto",
      centeredSlides: true,
      loop: true
    })

    this.projectSliderPrevBtn.addEventListener('click', e => {
      projectSlider.slidePrev()
    })

    this.projectSliderNextBtn.addEventListener('click', e => {
      projectSlider.slideNext()
    })

    this.projectSliderSlides.forEach(slide => {
      const projectGalleryOverlayToggle = slide.querySelector('[data-component="project-gallery-overlay-toggle"]')
      const slideIndex = projectGalleryOverlayToggle.getAttribute('data-project-gallery-overlay-slide-id')
      projectGalleryOverlayToggle.addEventListener('click', e => {
        this.projectOverlay.classList.add('visible')
        projectOverlaySlider.slideTo(slideIndex - 1)
      })
    })

    this.projectOverlaySliderPrevBtn.addEventListener('click', e => {
      projectOverlaySlider.slidePrev()
    })

    this.projectOverlaySliderNextBtn.addEventListener('click', e => {
      projectOverlaySlider.slideNext()
    })

    this.projectOverlayCloseBtns.forEach(button => {
      button.addEventListener('click', e => {
        this.projectOverlay.classList.remove('visible')
      })
    })

    if (this.SketchesSliderSwiper) {
      this.SketchesSliderSlides = this.SketchesSliderSwiper.querySelectorAll('.swiper-slide')
      this.SketchesSliderPrevBtn = this.SketchesSliderSwiper.querySelector('[data-component="swiper-button-prev"]')
      this.SketchesSliderNextBtn = this.SketchesSliderSwiper.querySelector('[data-component="swiper-button-next"]')
      this.SketchesOverlay = this.element.querySelector('[data-component="project-gallery-overlay"]#sketches-gallery')
      this.SketchesOverlaySlider = this.SketchesOverlay.querySelector('.sketches-gallery-overlay-slider')
      this.SketchesOverlayPrevBtn = this.SketchesOverlay.querySelector('[data-component="swiper-button-prev"]')
      this.SketchesOverlayNextBtn = this.SketchesOverlay.querySelector('[data-component="swiper-button-next"]')
      this.SketchesOverlayCloseBtns = this.SketchesOverlay.querySelectorAll('[data-component="close-sketches-gallery-overlay"]')

      var SketchesSlider = new Swiper(this.SketchesSliderSwiper, {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 50,
        autoHeight: true
      })

      var SketchesOverlaySlider = new Swiper(this.SketchesOverlaySlider, {
        slidesPerView: "auto",
        centeredSlides: true,
        loop: true
      })

      this.SketchesSliderSlides.forEach(slide => {
        const projectGalleryOverlayToggle = slide.querySelector('[data-component="project-gallery-overlay-toggle"]')
        const slideIndex = projectGalleryOverlayToggle.getAttribute('data-project-sketches-gallery-overlay-slide-id')
        projectGalleryOverlayToggle.addEventListener('click', e => {
          this.SketchesOverlay.classList.add('visible')
          SketchesOverlaySlider.slideTo(slideIndex - 1)
        })
      })

      this.SketchesSliderPrevBtn.addEventListener('click', e => {
        SketchesSlider.slidePrev()
      })

      this.SketchesSliderNextBtn.addEventListener('click', e => {
        SketchesSlider.slideNext()
      })

      this.SketchesOverlayPrevBtn.addEventListener('click', e => {
        SketchesOverlaySlider.slidePrev()
      })

      this.SketchesOverlayNextBtn.addEventListener('click', e => {
        SketchesOverlaySlider.slideNext()
      })

      this.SketchesOverlayCloseBtns.forEach(button => {
        button.addEventListener('click', e => {
          this.SketchesOverlay.classList.remove('visible')
        })
      })

    }
  }
}
