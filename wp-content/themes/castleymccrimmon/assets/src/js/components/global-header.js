/**
 * GlobalHeader functionality.
 * @author Dayne Coulson-Hoffacker
 */

export default class GlobalHeader {
  /**
     * Constructs a new GlobalHeader instance.
     * @param {Object} options - Options.
     * @param {HTMLElement} options.element - Container.
     */

  constructor(options) {
    this.element = options.element
    this.hasMainNavLink = this.element.querySelector('.has-main-nav')
    this.navToggle = this.element.querySelector('[data-component="nav-toggle"]')

    this.navToggle.addEventListener('click', e => {
      document.body.classList.toggle('nav-open')
    })
  }
}
