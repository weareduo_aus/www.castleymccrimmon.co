import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'
import { ScrollToPlugin } from 'gsap/ScrollToPlugin'
import Swiper from 'swiper/bundle'
gsap.registerPlugin(ScrollToPlugin)
gsap.registerPlugin(ScrollTrigger)

/**
 * HomeHeroSlider functionality.
 * @author Dayne Coulson-Hoffacker
 */

export default class HomeHeroSlider {
  /**
     * Constructs a new HomeHeroSlider instance.
     * @param {Object} options - Options.
     * @param {HTMLElement} options.element - Container.
     */

  constructor(options) {
    this.element = options.element
    this.scrollTrigger = this.element.nextElementSibling
    this.scrollBtn = this.element.querySelector('[data-component="scroll-btn"]')
    this.globalHeader = document.querySelector('[data-component="global-header"]')
    this.globalHeaderHeight = this.globalHeader.clientHeight
    this.swiper = this.element.querySelector('.swiper')
    this.slides = this.swiper.querySelectorAll('.swiper-slide')
    this.swiperPagination = this.swiper.querySelector('[data-component="slider-pagination"]')
    this.swiperButtonPrev = this.swiper.querySelector('[data-component="swiper-button-prev"]')
    this.swiperButtonNext = this.swiper.querySelector('[data-component="swiper-button-next"]')

    if (this.slides.length > 1) {
      const homeSlider = new Swiper(this.swiper, {
        loop: true,
        slidesPerView: 1
      })
      this.swiper.classList.add('show-controls')
      this.swiperButtonPrev.addEventListener('click', () => {
        homeSlider.slidePrev()
      })

      this.swiperButtonNext.addEventListener('click', () => {
        homeSlider.slideNext()
      })
    } else {
      const homeSlider = new Swiper(this.swiper, {
        loop: false,
        slidesPerView: 1
      })
    }

    this.scrollBtn.addEventListener('click', () => {
      gsap.to(window, { duration: 1, ease: 'power2', scrollTo: { y: this.scrollTrigger } })
    })

    ScrollTrigger.create({
      trigger: this.scrollTrigger,
      start: 'top 0',
      end: 'top 100%',
      markers: false,
      scroller: 'body',
      onEnter: () => {
        document.body.classList.add('hide-logo')
      },
      onLeaveBack: (direction) => {
        document.body.classList.remove('hide-logo')
      }
    })
  }
}