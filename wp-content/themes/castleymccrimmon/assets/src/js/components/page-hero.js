import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'
import { ScrollToPlugin } from "gsap/ScrollToPlugin"
gsap.registerPlugin(ScrollToPlugin)
gsap.registerPlugin(ScrollTrigger)

/**
 * PageHero functionality.
 * @author Dayne Coulson-Hoffacker
 */

 export default class PageHero {
  /**
     * Constructs a new PageHero instance.
     * @param {Object} options - Options.
     * @param {HTMLElement} options.element - Container.
     */

  constructor(options) {
    this.element = options.element
    this.scrollTrigger = this.element.querySelector('.column.text .inner')
    console.log(this.scrollTrigger);

    ScrollTrigger.create({
      trigger: this.scrollTrigger,
      start: 'top 0',
      end: 'top 100%',
      markers: false,
      scroller: 'body',
      onEnter: () => {
        document.body.classList.add('hide-logo')
      },
      onLeaveBack: () => {
        document.body.classList.remove('hide-logo')
      }
    })
  }
}