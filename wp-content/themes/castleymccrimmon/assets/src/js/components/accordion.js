export default class Accordion {
  /**
  * Constructs a new Accordion expander.
  * @param {object} options - Options.
  */
  constructor (options) {
    this.element = options.element
    var acc = this.element.queryAll(".accordion");
    var panel = this.element.queryAll('.panel');
    for (var i = 0; i < acc.length; i++) {
      acc[i].onclick = function() {
        var setClasses = !this.classList.contains('active');
        setClass(acc, 'active', 'remove');
        setClass(panel, 'active', 'remove');
        if (setClasses) {
          this.classList.toggle("active");
          this.nextElementSibling.classList.toggle("active");
        }
      }
    }
    function setClass(els, className, fnName) {
      for (var i = 0; i < els.length; i++) {
        els[i].classList[fnName](className);
      }
    }
  }
}
