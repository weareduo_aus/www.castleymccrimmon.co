import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'
import { ScrollToPlugin } from 'gsap/ScrollToPlugin'
gsap.registerPlugin(ScrollTrigger)
gsap.registerPlugin(ScrollToPlugin)

/**
 * FullScreenPageHero functionality.
 * @author Dayne Coulson-Hoffacker
 */

 export default class FullScreenPageHero {
  /**
     * Constructs a new FullScreenPageHero instance.
     * @param {Object} options - Options.
     * @param {HTMLElement} options.element - Container.
     */

  constructor(options) {
    this.element = options.element
    this.scrollTrigger = this.element.nextElementSibling
    this.scrollBtn = this.element.querySelector('[data-component="scroll-btn"]')
    this.globalHeader = document.querySelector('[data-component="global-header"]')
    this.globalHeaderHeight = this.globalHeader.clientHeight

    this.scrollBtn.addEventListener('click', () => {
      gsap.to(window, {duration: 1, ease: 'power2', scrollTo: {y: this.scrollTrigger }})
    })

    ScrollTrigger.create({
      trigger: this.scrollTrigger,
      start: 'top 0',
      end: 'top 100%',
      markers: false,
      scroller: 'body',
      onEnter: () => {
        document.body.classList.add('hide-logo')
      },
      onLeaveBack: (direction) => {
        document.body.classList.remove('hide-logo')
      }
    })
  }
}