import '../sass/app.scss';

import 'dom4'
import domready from 'domready'
import GlobalHeader from './components/global-header'
import HomeHeroSlider from './components/home-hero-slider'
import FullScreenPageHero from './components/full-screen-page-hero'
import PageHero from './components/page-hero'
import Accordion from './components/accordion'
import TestimonialsSlider from './components/testimonials-slider'
import Project from './components/project'
import GlobalFooter from './components/global-footer'

const touchDevice = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)
const globalHeader = document.querySelector('[data-component="global-header"]')

domready(() => {

  const appHeight = () => {
    const doc = document.documentElement
    doc.style.setProperty('--app-height', `${window.innerHeight}px`)
  }
  window.addEventListener('resize', appHeight)
  appHeight()

  const globalHeaderHeight = () => {
    const doc = document.documentElement
    const height = globalHeader.offsetHeight
    doc.style.setProperty('--global-header-height', `${height}px`)
  }
  window.addEventListener('resize', globalHeaderHeight)
  globalHeaderHeight()

  if (touchDevice) {
    document.body.classList.add('touch')
  }

  history.scrollRestoration = 'manual'

  window.scrollTo(0, 0)

  document.queryAll('[data-component="global-header"]').forEach(element => {
    new GlobalHeader({
      element
    })
  })

  document.queryAll('[data-component="home-hero-slider-module"]').forEach(element => {
    new HomeHeroSlider({
      element
    })
  })

  document.queryAll('[data-component="full-screen-page-hero"]').forEach(element => {
    new FullScreenPageHero({
      element
    })
  })

  document.queryAll('[data-component="page-hero"]').forEach(element => {
    new PageHero({
      element
    })
  })

  document.queryAll('[data-component="testimonials-slider"]').forEach(element => {
    new TestimonialsSlider({
      element
    })
  })

  document.queryAll('[data-component="project"]').forEach(element => {
    new Project({
      element
    })
  })

  document.queryAll('[data-component="accordion"]').forEach(element => {
    new Accordion({
      element
    })
  })

  document.queryAll('[data-component="global-footer"]').forEach(element => {
    new GlobalFooter({
      element
    })
  })

})
