<?php
/**
* The template for displaying projects
*/

get_header(); ?>

<?php get_template_part( 'template-parts/sections/_section', 'full-screen-page-hero' ); ?>

<?php if (have_posts()): ?>
  <section data-component="projects-module" class="default-module">
  <div class="container">
    <div class="column text">
      <div class="inner">
        <header>
          <h2 class="module-heading">Our Work</h2>
        </header>
        <?php
        $terms = get_terms([
          'taxonomy' => 'project_category',
          'hide_empty' => true,
        ]);
        ?>
        <ul data-component="project-categories">
          <li <?php if (is_post_type_archive('projects')): ?>class="cat-item current-cat"<?php endif; ?>><a href="/projects/">All</a></li>
        <?php foreach ($terms as $term):
          $class = ( is_tax('project_category', $term->slug) ) ? 'cat-item current-cat' : 'cat-item';
          ?>
          <li class="<?php echo $class; ?>"><a href="/project-category/<?php echo $term->slug; ?>"><?php echo $term->name; ?></a></li>
        <?php endforeach; ?>
        </ul>
      </div>
    </div>
    <div class="column projects">
      <header>
        <h3 class="module-heading">Projects</h3>
      </header>
      <ul class="projects">
      <?php
      $qobj = get_queried_object();
      $projects = array(
        'post_type' => 'projects',
        'posts_per_page' => -1
      );

      $loop = new WP_Query( $projects );

      while ( $loop->have_posts() ) : $loop->the_post(); ?>

        <?php get_template_part( 'template-parts/content', 'project' ); ?>

      <?php endwhile; ?>
      <?php wp_reset_query(); ?>
      </ul>
    </div>
  </div>
</section>
<?php endif; ?>

<?php get_footer(); ?>