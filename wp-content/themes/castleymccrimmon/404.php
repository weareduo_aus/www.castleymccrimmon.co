<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<section data-component="error-404">
  <div class="inner">
    <header class="page-title">
      <h1>404 Error</h1>
    </header>
    <header class="sub-title">
      <h3>Uh oh, how did you get here?</h3>
    </header>
    <p><a href="/" class="button">Take me home</a></p>
  </div>
</section>

<?php get_footer(); ?>
