<?php
/**
 * The main template file
 */

get_header(); ?>

<?php get_template_part( 'template-parts/sections/_section', 'page-hero' ); ?>

<?php if (have_posts()): ?>
  <section data-component="blog">
    <div class="container">
      <ul>

        <?php while ( have_posts() ) : the_post(); ?>

          <?php get_template_part( 'template-parts/content', 'single' ); ?>

        <?php endwhile; ?>

      </ul>

      <?php the_posts_pagination( array(
        'prev_text'          => __( '<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/wp-content/themes/collinscosmeticclinic/components/app/icons.svg#slider-arrow"></use></svg>', 'twentysixteen' ),
        'next_text'          => __( '<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/wp-content/themes/collinscosmeticclinic/components/app/icons.svg#slider-arrow"></use></svg>', 'twentysixteen' ),
        'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
      ) ); ?>

    </div>

  </section>

<?php endif; ?>

<?php get_footer(); ?>
