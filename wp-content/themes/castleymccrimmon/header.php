<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="google-site-verification" content="1Dbm6dn0-gf0d2Qg_rpt8LwnYUu6-3vGIHoEN_UjxnM" />
  <link rel="icon" type="image/svg+xml" href="<?php echo get_template_directory_uri(); ?>/assets/src/images/favicon.svg">
  <link rel="alternate icon" href="<?php echo get_template_directory_uri(); ?>/assets/src/images/favicon.png">
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-208868278-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-208868278-1');
  </script>
  <?php wp_head(); ?>
</head>

<?php
$page_background_colour = get_field('page_background_colour');
?>

<?php if ($page_background_colour == '6F7862'): ?>
  <body <?php body_class('green-colour-scheme'); ?> style="background-color: #<?php echo $page_background_colour; ?>; <?php if ($page_background_colour == '6F7862'): ?>color: white;<?php endif; ?>">
<?php else :?>
  <body <?php body_class(); ?> style="background-color: #<?php echo $page_background_colour; ?>; <?php if ($page_background_colour == '6F7862'): ?>color: white;<?php endif; ?>">
<?php endif; ?>

  <?php get_template_part( 'template-parts/components/_component', 'browser-warning' ); ?>

  <?php get_template_part('template-parts/global-header', 'none'); ?>