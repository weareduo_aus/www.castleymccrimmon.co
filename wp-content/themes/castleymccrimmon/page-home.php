<?php
/**
* Template Name: Home
*/

get_header(); ?>

<?php get_template_part( 'template-parts/sections/_section', 'home-hero-slider-module' ); ?>

<?php get_template_part( 'template-parts/sections/_section', 'page-builder' ); ?>

<?php get_footer(); ?>
