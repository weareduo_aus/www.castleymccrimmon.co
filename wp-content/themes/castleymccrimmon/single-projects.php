<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header();

$categories = get_the_category();
$project_location = get_field('project_location');
$project_year = get_field('project_year');
$project_description_first_column = get_field('project_description_first_column');
$project_description_second_column = get_field('project_description_second_column');
?>

<?php get_template_part( 'template-parts/sections/_section', 'full-screen-page-hero' ); ?>

<section data-component="project" class="default-module">
  <div class="container">
    <div class="column text">
      <div class="inner">
        <header>
          <h2 class="module-heading"><?php the_title(); ?></h2>
        </header>
        <p>—</p>
        <p><?php echo $project_location; ?></p>
        <div class="back-to">
          <a href="/projects/" class="back-to">
            <svg>
              <use xmlns:xlink="http://www.w3.org/1999/xlink"
                xlink:href="<?php echo get_template_directory_uri(); ?>/assets/src/images/icons.svg#arrow-icon">
              </use>
            </svg>
            <span>Back to projects</span>
          </a>
        </div>
      </div>
    </div>
    <div class="column project-content">
      <div class="row project-year">
        <p><?php echo $project_year; ?></p>
      </div>
      <div class="row project-description">
        <div class="column first">
          <?php echo $project_description_first_column; ?>
        </div>
        <div class="column last">
          <?php echo $project_description_second_column; ?>
        </div>
      </div>
      <?php if( have_rows('project_gallery') ): ?>
      <div class="row project-gallery">
        <div data-component="project-slider">
          <div class="swiper project-slider">
            <div class="swiper-wrapper">
              <?php $total_images = count( get_field( 'project_gallery' ) ); ?>
              <?php while( have_rows('project_gallery') ): the_row();
              $project_gallery_image = get_sub_field('project_gallery_image');
              $project_gallery_image_title = get_sub_field('project_gallery_image_title');
              $project_gallery_image_orientation = get_sub_field('project_gallery_image_orientation');
              $project_gallery_button_colour = get_sub_field('project_gallery_button_colour');
              ?>

              <div class="swiper-slide <?php echo $project_gallery_image_orientation; ?>">
                <button data-component="project-gallery-overlay-toggle"
                  data-project-gallery-overlay-slide-id="<?php echo get_row_index(); ?>"
                  class="<?php echo $project_gallery_button_colour; ?>">
                  <svg>
                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                      xlink:href="<?php echo get_template_directory_uri(); ?>/assets/src/images/icons.svg#plus-icon">
                    </use>
                  </svg>
                </button>
                <img src="<?php echo $project_gallery_image['url']; ?>"
                  alt="<?php echo $project_gallery_image_title; ?>">
                <div class="project-gallery-image-title">
                  <p><span><?php echo get_row_index(); ?> / <?php echo $total_images; ?></span></p>
                </div>
              </div>
              <?php endwhile; ?>
            </div>
            <button data-component="swiper-button-prev" class="slider-navigation-button"></button>
            <button data-component="swiper-button-next" class="slider-navigation-button"></button>
          </div>
        </div>
        <?php if( have_rows('project_credits') ): ?>
          <ul class="project-credits">
          <?php while( have_rows('project_credits') ): the_row(); 
              $project_credit_type = get_sub_field('project_credit_type');
              $project_credit_name = get_sub_field('project_credit_name');
              ?>
              <li>
                <p><?php echo $project_credit_type; ?></p>
                <p><?php echo $project_credit_name; ?></p>
              </li>
          <?php endwhile; ?>
          </ul>
        <?php endif; ?>
      </div>
      <?php endif; ?>
      <?php if( have_rows('project_sketches_gallery_new') ): ?>
      <div class="row project-sketches-gallery">
        <div data-component="project-slider">
          <div class="swiper project-sketches-slider">
            <div class="swiper-wrapper">
              <?php $total_images = count( get_field( 'project_sketches_gallery_new' ) ); ?>
              <?php while( have_rows('project_sketches_gallery_new') ): the_row();
              $project_sketches_gallery_image = get_sub_field('project_sketches_gallery_image');
              $project_sketches_gallery_image_title = get_sub_field('project_sketches_gallery_image_title');
              $project_sketches_gallery_image_orientation = get_sub_field('project_sketches_gallery_image_orientation');
              ?>

              <div class="swiper-slide <?php echo $project_sketches_gallery_image_orientation; ?>">
                <button data-component="project-gallery-overlay-toggle"
                  data-project-sketches-gallery-overlay-slide-id="<?php echo get_row_index(); ?>">
                  <svg>
                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                      xlink:href="<?php echo get_template_directory_uri(); ?>/assets/src/images/icons.svg#plus-icon">
                    </use>
                  </svg>
                </button>
                <img src="<?php echo $project_sketches_gallery_image['url']; ?>"
                  alt="<?php echo $project_sketches_gallery_image_title; ?>">
                <div class="project-gallery-image-title">
                  <p><span><?php echo get_row_index(); ?> / <?php echo $total_images; ?></span></p>
                </div>
              </div>
              <?php endwhile; ?>
            </div>
            <button data-component="swiper-button-prev" class="slider-navigation-button"></button>
            <button data-component="swiper-button-next" class="slider-navigation-button"></button>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <div class="row projects-pagination">
        <?php
        the_post_navigation(
          array(
            'next_text' => '<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' . get_template_directory_uri() . '/assets/src/images/icons.svg#arrow-icon"></use></svg><span>%title</span>',
            'prev_text' => '<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' . get_template_directory_uri() . '/assets/src/images/icons.svg#arrow-icon"></use></svg><span>%title</span>',
          )
        );
        ?>
      </div>
    </div>
  </div>
  <?php get_template_part( 'template-parts/components/_component', 'project-gallery-overlay' ); ?>
  <?php get_template_part( 'template-parts/components/_component', 'project-sketches-gallery-overlay' ); ?>
</section>

<?php get_footer(); ?>